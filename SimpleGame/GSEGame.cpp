#include "stdafx.h"
#include "GSEGame.h"
#include "GSEObject.h"

GSEGame::GSEGame(GSEVec2 size)
{
	m_Renderer = new Renderer((int)size.x, (int)size.y);
	m_objectMgr = new GSEObjectMgr();
	m_soundMgr = new Sound();

	// Create Textures
	m_heroTextureID = m_Renderer->GenPngTexture("./Texture/Player.png");
	m_bullet01TextureID = m_Renderer->GenPngTexture("./Texture/Bullet01.png");
	m_bullet02TextureID = m_Renderer->GenPngTexture("./Texture/Bullet02.png");
	m_fireAnimTextureID = m_Renderer->GenPngTexture("./Texture/FireSheet.png");
	m_preyTextureID = m_Renderer->GenPngTexture("./Texture/Prey.png");
	m_groundTextureID = m_Renderer->GenPngTexture("./Texture/newbg.png");

	// Create Sound
	m_bgSound = m_soundMgr->CreateBGSound("./Sounds/frogsong.mp3");
	m_explSound = m_soundMgr->CreateShortSound("./Sounds/explosion.mp3");
	m_fireSound = m_soundMgr->CreateShortSound("./Sounds/firing.mp3");
	m_preySound = m_soundMgr->CreateShortSound("./Sounds/eat.mp3");

	m_soundMgr->PlayBGSound(m_bgSound, true, m_soundVolume);

	// Create Particle Obj
	m_climateParticle = m_Renderer->CreateParticleObject(1000, -1024, -1024, 1024, 1024,
		3, 3, 7, 7, -3, -3, 3, 0);

	m_fireParticle = m_Renderer->CreateParticleObject(1000, 0, 0, 0, 0,
		3, 3, 7, 7, -3, -3, 3, 3);

	// Create hero object
	GSEVec3 heroObjPos = { 0, 0, 10 };
	GSEVec3 heroObjSize = { 40, 40, 20 };
	GSEVec3 heroObjVel = { 0, 0, 0 };
	GSEVec3 heroObjAcc = { 0, 0, 0 };
	float heroObjMass = 1.f;
	int heroHP = 3000;
	m_heroID = m_objectMgr->AddObject(heroObjPos, heroObjSize, heroObjVel, heroObjAcc, heroObjMass);
	m_objectMgr->GetObject(m_heroID)->SetType(OBJ_TYPE_HERO);
	m_objectMgr->GetObject(m_heroID)->SetHP(heroHP);
	m_objectMgr->GetObject(m_heroID)->SetMaxHP(heroHP);
	m_objectMgr->GetObject(m_heroID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(m_heroID)->SetTextureID(m_heroTextureID);

	// Create test objects
	GSEVec3 testObjPos = { 100, 0, 0 };
	GSEVec3 testObjSize = { 40, 40, 20 };
	GSEVec3 testObjVel = { 0, 0, 0 };
	GSEVec3 testObjAcc = { 0, 0, 0 };
	float testObjMass = 1.f;
	int testHP = 300;
	int testObjID = m_objectMgr->AddObject(testObjPos, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObjID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObjID)->SetHP(testHP);
	m_objectMgr->GetObject(testObjID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObjID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObjID)->SetTextureID(m_fireAnimTextureID);

	int testObj2ID = m_objectMgr->AddObject({ 100, 100, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj2ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj2ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj2ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj2ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj2ID)->SetTextureID(m_fireAnimTextureID);

	int testObj3ID = m_objectMgr->AddObject({ -100, -100, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj3ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj3ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj3ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj3ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj3ID)->SetTextureID(m_fireAnimTextureID);

	int testObj4ID = m_objectMgr->AddObject({ -200, -50, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj4ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj4ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj4ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj4ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj4ID)->SetTextureID(m_fireAnimTextureID);

	// Add test objects
	int testObj5ID = m_objectMgr->AddObject({ 230, 70, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj5ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj5ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj5ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj5ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj5ID)->SetTextureID(m_fireAnimTextureID);

	int testObj6ID = m_objectMgr->AddObject({ -300, -20, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj6ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj6ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj6ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj6ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj6ID)->SetTextureID(m_fireAnimTextureID);

	int testObj7ID = m_objectMgr->AddObject({ -230, 50, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj7ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj7ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj7ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj7ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj7ID)->SetTextureID(m_fireAnimTextureID);

	int testObj8ID = m_objectMgr->AddObject({ 200, -50, 0 }, testObjSize, testObjVel, testObjAcc, testObjMass);
	m_objectMgr->GetObject(testObj8ID)->SetType(OBJ_TYPE_NORMAL);
	m_objectMgr->GetObject(testObj8ID)->SetHP(testHP);
	m_objectMgr->GetObject(testObj8ID)->SetMaxHP(testHP);
	m_objectMgr->GetObject(testObj8ID)->SetBDrawGauge(true);
	m_objectMgr->GetObject(testObj8ID)->SetTextureID(m_fireAnimTextureID);

	// Create prey object
	GSEVec3 preyObjPos = { 220, -140, 0 };
	GSEVec3 preyObjSize = { 30, 30, 20 };
	GSEVec3 preyObjVel = { 0, 0, 0 };
	GSEVec3 preyObjAcc = { 0, 0, 0 };
	float preyObjMass = 1.f;
	int preyHP = 100;
	int preyObj1ID = m_objectMgr->AddObject(preyObjPos, preyObjSize, preyObjVel, preyObjAcc, preyObjMass);
	m_objectMgr->GetObject(preyObj1ID)->SetType(OBJ_TYPE_PREY);
	m_objectMgr->GetObject(preyObj1ID)->SetHP(preyHP);
	m_objectMgr->GetObject(preyObj1ID)->SetMaxHP(preyHP);
	m_objectMgr->GetObject(preyObj1ID)->SetParent(m_objectMgr->GetObject(m_heroID));
	m_objectMgr->GetObject(preyObj1ID)->SetTextureID(m_preyTextureID);

	int preyObj2ID = m_objectMgr->AddObject({ -130, 170 }, preyObjSize, preyObjVel, preyObjAcc, preyObjMass);
	m_objectMgr->GetObject(preyObj2ID)->SetType(OBJ_TYPE_PREY);
	m_objectMgr->GetObject(preyObj2ID)->SetHP(preyHP);
	m_objectMgr->GetObject(preyObj2ID)->SetMaxHP(preyHP);
	m_objectMgr->GetObject(preyObj2ID)->SetParent(m_objectMgr->GetObject(m_heroID));
	m_objectMgr->GetObject(preyObj2ID)->SetTextureID(m_preyTextureID);
}

GSEGame::~GSEGame()
{
	if (m_Renderer) {
		delete m_Renderer;
	}

	if (m_objectMgr) {
		delete m_objectMgr;
	}

	if (m_heroTextureID >= 0) {
		m_Renderer->DeleteTexture(m_heroTextureID);
	}
	if (m_bullet01TextureID >= 0) {
		m_Renderer->DeleteTexture(m_bullet01TextureID);
	}
	if (m_bullet02TextureID >= 0) {
		m_Renderer->DeleteTexture(m_bullet02TextureID);
	}
	if (m_fireAnimTextureID >= 0) {
		m_Renderer->DeleteTexture(m_fireAnimTextureID);
	}
	if (m_preyTextureID >= 0) {
		m_Renderer->DeleteTexture(m_preyTextureID);
	}
	if (m_groundTextureID >= 0) {
		m_Renderer->DeleteTexture(m_groundTextureID);
	}

	if (m_bgSound >= 0) {
		m_soundMgr->DeleteBGSound(m_bgSound);
	}
	if (m_explSound >= 0) {
		m_soundMgr->DeleteBGSound(m_explSound);
	}
	if (m_fireSound >= 0) {
		m_soundMgr->DeleteBGSound(m_fireSound);
	}
	if (m_preySound >= 0) {
		m_soundMgr->DeleteBGSound(m_preySound);
	}
}

void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	// 배경
	m_Renderer->DrawGround(0, 0, 0,
		1200 * 5, 667 * 5, 0, 1, 1, 1, 1, m_groundTextureID);

	// Renderer Test
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		GSEObject* temp = m_objectMgr->GetObject(i);

		if (temp != NULL) {
			GSEVec3 pos = temp->GetPos();
			GSEVec3 size = temp->GetSize();
			GSEVec4 color = temp->GetColor();

			if (size.x > 0.0000001f) {
				// m_Renderer->DrawSolidRect(pos.x, pos.y, pos.z, size.x, color.x, color.y, color.z, color.w);
				//m_Renderer->DrawSolidRect(pos.x, pos.y, pos.z, 
				//	size.x, size.y, size.z, color.x, color.y, color.z, color.w);
				int textureID = temp->GetTextureID();
				if (textureID < 0) {
					m_Renderer->DrawSolidRect(pos.x, pos.y, pos.z,
						size.x, size.y, size.z, color.x, color.y, color.z, color.w);
				}
				else if (textureID == m_fireAnimTextureID) {
					float age = temp->GetAge();
					int fps = 24.f;
					int currX = (int)(age * fps) % 10;
					int currY = (int)((age * fps) / 10.f);
					m_Renderer->DrawTextureRectAnim(pos.x, pos.y, pos.z,
						size.x, size.y, size.z, color.x, color.y, color.z, color.w,
						textureID, 5, 3, currX, currY, false);
				}
				else {
				m_Renderer->DrawTextureRect(pos.x, pos.y, pos.z, 
					size.x, size.y, size.z, color.x, color.y, color.z, color.w,
					textureID);
				}
			}

			bool bDrawGauge = temp->GetBDrawGauge();
			if (bDrawGauge) {
				int maxHP = temp->GetMaxHP();
				int HP = temp->GetHP();
				float percent = 100.f * (float)HP / (float)maxHP;
				m_Renderer->DrawSolidRectGauge(pos.x, pos.y, pos.z,
					0, size.y / 2, 0, size.x, 2, 10,
					1, 0, 0, 1, (int)percent, false);
			}
		}
	}

	m_Renderer->DrawParticle(m_climateParticle,
		0, 0, 0, 1, 1, 1, 1, 1, -10, -10,
		m_bullet02TextureID, 1.f,
		m_objectMgr->GetObject(m_heroID)->GetAge());;

	//m_Renderer->DrawParticle(m_fireParticle,
	//	0, 0, 0, 1, 1, 1, 1, 1, 0, 0,
	//	m_bullet01TextureID, 1.f,
	//	m_objectMgr->GetObject(m_heroID)->GetAge());;
}

void GSEGame::UpdateObjects(GSEKeyboardMapper keyMap, float eTime)
{
	// Add force
	if (keyMap.W_Key || keyMap.A_Key || keyMap.S_Key || keyMap.D_Key) {
		float movingHeroForce = 1000.f;
		GSEVec3 heroForceDirection = { 0.f, 0.f, 0.f };
		if (keyMap.W_Key) {
			heroForceDirection.y = 1.f;
		}
		if (keyMap.A_Key) {
			heroForceDirection.x = -1.f;
		}
		if (keyMap.S_Key) {
			heroForceDirection.y = -1.f;
		}
		if (keyMap.D_Key) {
			heroForceDirection.x = 1.f;
		}

		heroForceDirection.x *= movingHeroForce;
		heroForceDirection.y *= movingHeroForce;
		heroForceDirection.z *= movingHeroForce;

		m_objectMgr->AddForce(m_heroID, heroForceDirection, eTime);
	}

	// Fire bullet
	bool b_canFire = m_objectMgr->GetObject(m_heroID)->CanFire();

	if ( (keyMap.Up_Key || keyMap.Down_Key || keyMap.Left_Key || keyMap.Right_Key) && b_canFire ) {
		float firingBulletForce = 1000.f;

		GSEVec3 bulletForceDirection;
		if (keyMap.Up_Key) {
			bulletForceDirection.y += 1.f;
		}
		if (keyMap.Left_Key) {
			bulletForceDirection.x -= 1.f;
		}
		if (keyMap.Down_Key) {
			bulletForceDirection.y -= 1.f;
		}
		if (keyMap.Right_Key) {
			bulletForceDirection.x += 1.f;
		}
		bulletForceDirection.x *= firingBulletForce;
		bulletForceDirection.y *= firingBulletForce;
		bulletForceDirection.z *= firingBulletForce;
	
		if (FLT_EPSILON < std::fabs(bulletForceDirection.x) + std::fabs(bulletForceDirection.y) +
			std::fabs(bulletForceDirection.z)) {
			GSEVec3 bulletObjPos = { 0, 0, 0 };
			GSEVec3 bulletObjSize = { 40, 40, 10 };
			GSEVec3 bulletObjVel = { 0, 0, 0 };
			GSEVec3 bulletObjAcc = { 0, 0, 0 };
			float bulletObjMass = 0.1f;
			int bulletID = m_objectMgr->AddObject(bulletObjPos, bulletObjSize, bulletObjVel, bulletObjAcc, bulletObjMass);

			if (bulletID >= 0) {
				GSEVec3 heroPos;
				heroPos = m_objectMgr->GetObject(m_heroID)->GetPos();
				m_objectMgr->GetObject(bulletID)->SetPos(heroPos);	// 초기 위치
				m_objectMgr->GetObject(bulletID)->SetFricCoef(1.f);
				m_objectMgr->GetObject(bulletID)->SetType(OBJ_TYPE_BULLET);
				m_objectMgr->AddForce(bulletID, bulletForceDirection, 0.1f);

				// 총알의 parent 설정
				m_objectMgr->GetObject(bulletID)->SetParent(m_objectMgr->GetObject(m_heroID));
				m_objectMgr->GetObject(bulletID)->SetHP(100);
				m_objectMgr->GetObject(bulletID)->SetMaxHP(100);

				if (rand() % 2 == 0) {
					m_objectMgr->GetObject(bulletID)->SetTextureID(m_bullet01TextureID);
				}
				else {
					m_objectMgr->GetObject(bulletID)->SetTextureID(m_bullet02TextureID);
				}
				m_soundMgr->PlayShortSound(m_fireSound, false, m_soundVolume);

				// 쿨타임 리셋
				m_objectMgr->GetObject(m_heroID)->ResetCoolTime();
			}
		}
	}

	// Collision Test
	bool testResult[MAX_OBJECT_COUNT];
	memset(testResult, 0, sizeof(bool) * MAX_OBJECT_COUNT);

	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		for (int j = i + 1; j < MAX_OBJECT_COUNT; ++j) {
			if (m_objectMgr->GetObject(i) == NULL || m_objectMgr->GetObject(j) == NULL) {
				continue;
			}

			// Get min, max value of object
			GSEVec3 objASize = m_objectMgr->GetObject(i)->GetSize();
			GSEVec3 objAPos = m_objectMgr->GetObject(i)->GetPos();
			GSEVec3 objAMin = { objAPos.x - objASize.x / 2.f, objAPos.y - objASize.y / 2.f, objAPos.z - objASize.z/2.f };
			GSEVec3 objAMax = { objAPos.x + objASize.x / 2.f, objAPos.y + objASize.y / 2.f, objAPos.z + objASize.z/2.f };

			GSEVec3 objBSize = m_objectMgr->GetObject(j)->GetSize();
			GSEVec3 objBPos = m_objectMgr->GetObject(j)->GetPos();
			GSEVec3 objBMin = { objBPos.x - objBSize.x / 2.f, objBPos.y - objBSize.y / 2.f, objBPos.z - objBSize.z / 2.f };
			GSEVec3 objBMax = { objBPos.x + objBSize.x / 2.f, objBPos.y + objBSize.y / 2.f, objBPos.z + objBSize.z / 2.f };

			bool isCollide = BBCollision(objAMin, objAMax, objBMin, objBMax);
			bool isParent = m_objectMgr->GetObject(i)->IsAncester(m_objectMgr->GetObject(j)) ||
				m_objectMgr->GetObject(j)->IsAncester(m_objectMgr->GetObject(i));

			if (isCollide) {
				// 히어로랑 총알끼리는 충돌x
				if (!isParent) {
					GSEObject* A1 = m_objectMgr->GetObject(i);
					GSEObject* A2 = m_objectMgr->GetObject(j);
					float m1 = A1->GetMass();
					float m2 = A2->GetMass();
					GSEVec3 vel1 = A1->GetVel();
					GSEVec3 vel2 = A2->GetVel();

					float finalVelX1 = ((m1 - m2) / (m1 + m2)) * vel1.x + ((2.f * m2) / (m1 + m2) * vel2.x);
					float finalVelY1 = ((m1 - m2) / (m1 + m2)) * vel1.y + ((2.f * m2) / (m1 + m2) * vel2.y);
					float finalVelX2 = ((2.f * m1) / (m1 + m2)) * vel1.x + ((m2 - m1) / (m1 + m2) * vel2.x);
					float finalVelY2 = ((2.f * m1) / (m1 + m2)) * vel1.y + ((m2 - m1) / (m1 + m2) * vel2.y);
					GSEVec3 temp1{ finalVelX1, finalVelY1, vel1.z };
					GSEVec3 temp2{ finalVelX2, finalVelY2, vel1.z };
					A1->SetVel(temp1);
					A2->SetVel(temp2);

					// 데미지 처리
					int hp1 = A1->GetHP();
					int hp2 = A2->GetHP();
					A1->SetHP(hp1 - hp2);
					A2->SetHP(hp2 - hp1);

					testResult[i] = true;
					testResult[j] = true;

					m_soundMgr->PlayShortSound(m_explSound, false, m_soundVolume);
				}

				// 히어로랑 파리끼리는 체력 회복
				if (m_objectMgr->GetObject(i)->GetType() == OBJ_TYPE_HERO && m_objectMgr->GetObject(j)->GetType() == OBJ_TYPE_PREY) {
					int hp = m_objectMgr->GetObject(i)->GetHP();
					m_objectMgr->GetObject(i)->SetHP(hp + 500);
					m_objectMgr->GetObject(j)->SetHP(0);

					m_soundMgr->PlayShortSound(m_preySound, false, m_soundVolume);
				}
			}
		}
	}

	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		if (m_objectMgr->GetObject(i) != NULL) {
			if (testResult[i]) {
				GSEVec4 TrueColor = { 1, 0, 0, 1 };
				m_objectMgr->GetObject(i)->SetColor(TrueColor);
			}
			else {
				GSEVec4 FalseColor = { 1, 1, 1, 1 };
				m_objectMgr->GetObject(i)->SetColor(FalseColor);
			}
		}
	}

	// Update position
	if (m_objectMgr != NULL) {
		m_objectMgr->UpdateObjects(eTime);
	}
	else {
		std::cout << "m_objectMgr is NULL" << std::endl;
	}

	GSEVec3 heroPos;
	heroPos = m_objectMgr->GetObject(m_heroID)->GetPos();
	m_Renderer->SetCameraPos(heroPos.x, heroPos.y);

	m_objectMgr->DoGarbageCollect();
}

bool GSEGame::BBCollision(GSEVec3 minA, GSEVec3 maxA, GSEVec3 minB, GSEVec3 maxB)
{
	if (minA.x > maxB.x) {
		return false;
	}
	if (maxA.x < minB.x) {
		return false;
	}
	if (minA.y > maxB.y) {
		return false;
	}
	if (maxA.y < minB.y) {
		return false;
	}
	if (minA.z > maxB.z) {
		return false;
	}
	if (maxA.z < minB.z) {
		return false;
	}
	return true;
}

