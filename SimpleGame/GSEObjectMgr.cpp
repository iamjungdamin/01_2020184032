#include "stdafx.h"
#include "GSEObjectMgr.h"

GSEObjectMgr::GSEObjectMgr()
{
	// Init object list
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		m_objects[i] = NULL;
	}
}

GSEObjectMgr::~GSEObjectMgr()
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		DeleteObject(i);
	}
}

int GSEObjectMgr::AddObject(GSEVec3 pos, GSEVec3 size, GSEVec3 vel, GSEVec3 acc, float mass)
{
	// Find empty slot
	int index = FindEmptySlot();

	if (index < 0) {
		std::cout << "No more empty object slot" << std::endl;
		return -1;
	}

	m_objects[index] = new GSEObject(pos, size, vel, acc, mass);
	return index;
}

GSEObject* GSEObjectMgr::GetObject(int index)
{
	if (m_objects[index] != NULL) {
		return m_objects[index];
	}

	// log, 아니면 assertion
	//std::cout << "Object is NULL\n";
	return NULL;
}

bool GSEObjectMgr::DeleteObject(int index)
{
	if (m_objects[index]) {
		delete m_objects[index];
		m_objects[index] = NULL;
		return true;
	}

	return false;
}

int GSEObjectMgr::FindEmptySlot()
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		if (m_objects[i] == NULL) {
			return i;
		}
	}

	// Fail to find empty slot
	return -1;
}

void GSEObjectMgr::UpdateObjects(float eTime)
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		if (m_objects[i] != NULL) {
			m_objects[i]->Update(eTime);
		}
	}
}

void GSEObjectMgr::AddForce(int index, GSEVec3 force, float eTime)
{
	if (m_objects[index] != NULL) {
		m_objects[index]->AddForce(force, eTime);
	}
}

void GSEObjectMgr::DoGarbageCollect()
{
	for (int i = 0; i < MAX_OBJECT_COUNT; ++i) {
		if (m_objects[i] != NULL) {

			// Check destroy status
			// bullet: type, velocity
			if (m_objects[i]->GetType() == OBJ_TYPE_BULLET) {
				GSEVec3 vel = m_objects[i]->GetVel();
				float size = std::sqrtf(vel.x * vel.x + vel.y * vel.y);
				if (size < FLT_EPSILON) {
					// 속도가 0인 경우
					// Delete object
					DeleteObject(i);
				}
			}
		}

		if (m_objects[i] != NULL) {

			// Check destroy status
			int hp = m_objects[i]->GetHP();
			if (hp <= 0) {
				// hp가 0인 경우
				// Delete object
				DeleteObject(i);
			}
		}
	}
}

