#pragma once

#define MAX_OBJECT_COUNT 1000
#define GRAVITY 9.8f

struct GSEVec2 {
	float x = 0.f;
	float y = 0.f;
};

struct GSEVec3 {
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;
};

struct GSEVec4 {
	float x = 0.f;
	float y = 0.f;
	float z = 0.f;
	float w = 0.f;
};

struct GSEKeyboardMapper {
	bool W_Key = false;
	bool A_Key = false;
	bool S_Key = false;
	bool D_Key = false;
	bool Up_Key = false;
	bool Down_Key = false;
	bool Left_Key = false;
	bool Right_Key = false;
};

enum ObjType {
	OBJ_TYPE_NORMAL,
	OBJ_TYPE_HERO,
	OBJ_TYPE_BULLET,
	OBJ_TYPE_PREY
};
