#pragma once
#include "Global.h"
#include "Renderer.h"
#include "GSEObjectMgr.h"
#include "Sound.h"

class GSEGame
{
public:
	GSEGame(GSEVec2 size);
	~GSEGame();

	void RenderScene();
	void UpdateObjects(GSEKeyboardMapper keyMap, float eTime);

	bool BBCollision(GSEVec3 minA, GSEVec3 maxA, GSEVec3 minB, GSEVec3 maxB);
	
private:
	Renderer* m_Renderer = NULL;
	GSEObjectMgr* m_objectMgr = NULL;
	int m_heroID = -1;

	int m_heroTextureID = -1;
	int m_bullet01TextureID = -1;
	int m_bullet02TextureID = -1;
	int m_fireAnimTextureID = -1;
	int m_preyTextureID = -1;
	int m_groundTextureID = -1;

	Sound* m_soundMgr = NULL;
	float m_soundVolume = 1.0f;

	int m_bgSound = -1;
	int m_explSound = -1;
	int m_fireSound = -1;
	int m_preySound = -1;

	int m_climateParticle = -1;
	int m_fireParticle = -1;
};

